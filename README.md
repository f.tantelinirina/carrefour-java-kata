# Construction du projet (se positionner à la racine du projet) :
	- MacOs/Linux (chmod +x mvnw): ./mvnw clean install -DskipTests
	- Windows : mvnw.bat clean install -DskipTes
	- Ou si vous avez déjà un maven en local (avec MAVEN_HOME configurer et ajouter dans PATH) : mvn clean install

# security
	- spring security
	
# Persistence
	- ORM : hibernate(JPA)
	- cache : spring cache

# Database :
	- Sever : H2
	- adresse : 127.0.0.1
	- user : sa
	- password :

# API docs :
	- http://localhost:8080/api-docs
    - http://localhost:8080/swagger-ui/index.html

# Spring actuator :
	- http://localhost:8080/actuator

# Lancement du projet (se positionner à la racine du projet) :
	- MacOs/Linux  : ./mvnw spring-boot:run
	- Windows : mvnw.bat spring-boot:run

# API Rest
	- curl -u admin:password -X GET http://localhost:8080/api/customer -H "Accept: application/json"
	- curl -u admin:password -X GET http://localhost:8080/api/delivery-method -H "Accept: application/json"
	- curl -u admin:password -d '{"numeroOrder":"#1","deliveryMethod":"DRIVE","customerId":"1","deliveryDate":"2024-04-05 18:18:18","deliveryHour":"9"}' -H 'Content-Type: application/json'  http://localhost:8080/api/order