package com.carrefour.javakata.controller;

import com.carrefour.javakata.enums.DeliveryMethod;
import com.carrefour.javakata.exception.InternalServerException;
import com.carrefour.javakata.model.domain.Order;
import com.carrefour.javakata.model.dto.OrderDto;
import com.carrefour.javakata.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Tag(name = "OrderController", description = "Api Order /api/order")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api", produces = {MediaType.APPLICATION_JSON_VALUE})
public class OrderController {

    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    private final OrderService orderService;

    /**
     * Save the order of command for a customer with delivery information
     *
     * @param orderDto
     * @return OrderDto
     */
    @Operation(summary = "Save the order of command for a customer with delivery information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success operation", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = OrderDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access to this resource is denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource Not Found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)})
    @PostMapping("/order")
    public ResponseEntity<Order> registerOrder(@RequestBody @Parameter(name = "orderDto", description = "OrderDto : order information (JSON)", required = true) OrderDto orderDto) {
        try {
            return new ResponseEntity<>(orderService.save(orderDto), HttpStatus.CREATED);
        } catch (Exception ex) {
            throw new InternalServerException("Error registerOrder " + ex.getMessage());

        }
    }

    /**
     * Get list of order for a customer id
     *
     * @param customerId
     * @return CollectionModel<Order>
     */
    @Operation(summary = "Get list the order for hateos principle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success operation", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = OrderDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access to this resource is denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource Not Found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)})
    @ResponseBody
    @GetMapping(value = "/order/customer/{customer-id}")
    public CollectionModel<Order> getOrdersForCustomer(@PathParam("customer-id") @Parameter(name = "customerId", description = "customerId : customer ID", required = true) Long customerId) {
        return orderService.retrieveOrdersForCustomerId(customerId);
    }

    /**
     * Get list the order for a customer and delivery method
     *
     * @param customerId
     * @param deliveryMethod
     * @return CompletableFuture<List < OrderDto>>
     */
    @Operation(summary = "Get list the order for a customer and delivery method")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success operation", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = OrderDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access to this resource is denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource Not Found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)})
    @ResponseBody
    @GetMapping("/order/customer/{customer-id}/delivery/{delivery-method}")
    public CompletableFuture<List<OrderDto>> retrieveOrderForCustomerIdAndDeliveryMethod(
            @PathParam("customer-id") @Parameter(name = "customerId", description = "customerId : customer ID", required = true) Long customerId,
            @PathParam("delivery-method") @Parameter(name = "deliveryMethod", description = "deliveryMethod : delivery method", required = true) DeliveryMethod deliveryMethod) {
        try {
            return orderService.retrieveForCustomerIdAndDeliveryMethod(customerId, deliveryMethod);
        } catch (Exception ex) {
            throw new InternalServerException("Error retrieveOrderForCustomerIdAndDeliveryMethod " + ex.getMessage());
        }
    }


    /**
     * Get Order for customer and order id
     *
     * @param customerId
     * @param orderId
     * @return Order
     */
    @Operation(summary = "Get order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success operation", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = OrderDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access to this resource is denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource Not Found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)})
    @ResponseBody
    @GetMapping(value = "/order/{order-id}/customer/{customer-id}")
    public Order getOrderById(
            @PathParam("customer-id") @Parameter(name = "customerId", description = "customerId : customer ID", required = true) Long customerId,
            @PathParam("order-id") @Parameter(name = "orderId", description = "orderId : order ID", required = true) Long orderId) {
        return orderService.retrieveOrderForId(customerId, orderId);
    }
}
