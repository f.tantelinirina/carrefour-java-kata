package com.carrefour.javakata.controller;

import com.carrefour.javakata.enums.DeliveryMethod;
import com.carrefour.javakata.model.dto.OrderDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Tag(name = "DeliveryMethodController", description = "Api DeliveryMethod Controller /api/delivery-method")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api", produces = {MediaType.APPLICATION_JSON_VALUE})
public class DeliveryMethodController {

    @Operation(summary = "Get list of delivery method information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success operation", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = OrderDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access to this resource is denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Resource Not Found", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)})
    @GetMapping("/delivery-method")
    public ResponseEntity<List<DeliveryMethod>> getDeliveryMethods(){
        return ResponseEntity.ok(List.of(DeliveryMethod.values()));
    }

}
