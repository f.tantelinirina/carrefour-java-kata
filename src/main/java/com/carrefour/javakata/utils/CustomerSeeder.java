package com.carrefour.javakata.utils;

import com.carrefour.javakata.model.domain.Customer;
import com.carrefour.javakata.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Component
@RequiredArgsConstructor
public class CustomerSeeder implements CommandLineRunner {
    private final CustomerService customerService;

    @Override
    public void run(String... args) throws Exception {
        List<Customer> customerList = new ArrayList<>();
        for (int i = 0; i <= 9; i++) {
            Customer customer = BeanUtils.instantiateClass(Customer.class);
            customer.setFirstName("First Name #1");
            customer.setLastName("Last Name #1");
            customer.setFullAddress("Full adresse #1");
            customer.setCreationDate(Calendar.getInstance().getTime());
            customerList.add(customer);
        }
        customerService.save(customerList);
    }
}
