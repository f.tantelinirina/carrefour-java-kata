package com.carrefour.javakata.service;

import com.carrefour.javakata.controller.OrderController;
import com.carrefour.javakata.enums.DeliveryMethod;
import com.carrefour.javakata.exception.DataNotFoundException;
import com.carrefour.javakata.exception.InternalServerException;
import com.carrefour.javakata.mapper.OrderMapper;
import com.carrefour.javakata.model.domain.Order;
import com.carrefour.javakata.model.dto.OrderDto;
import com.carrefour.javakata.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"cacheOrders"})
public class OrderService {
    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);
    private final OrderRepository orderRepository;
    private final CustomerService customerService;


    /**
     * Save the order from OrderDto
     *
     * @param orderDto
     * @return Order
     * @throws InternalServerException
     */
    @Transactional
    @CacheEvict(key = "1")
    public Order save(OrderDto orderDto) throws InternalServerException {
        if (Objects.nonNull(orderDto.getCustomerId())) {
            try {
                Order order = BeanUtils.instantiateClass(Order.class);
                order.setCustomer(customerService.retrieveForId(orderDto.getCustomerId()));
                order.setNumero(orderDto.getNumeroOrder());
                order.setDeliveryDate(orderDto.getDeliveryDate());
                order.setDeliveryHour(orderDto.getDeliveryHour());
                order.setDeliveryMethod(orderDto.getDeliveryMethod());
                orderRepository.save(order);
                return order;
            } catch (DataNotFoundException ex) {
                logger.info(ex.getMessage());
                throw new InternalServerException("Error registerOrder " + ex.getMessage());
            }
        }
        throw new InternalServerException("Error registerOrder ");
    }


    /**
     * @param customerId
     * @param deliveryMethod
     * @return
     */
    @CachePut(key = "2")
    public CompletableFuture<List<OrderDto>> retrieveForCustomerIdAndDeliveryMethod(Long customerId, DeliveryMethod deliveryMethod) {
        try {
            CompletableFuture<List<Order>> orders = orderRepository.findByCustomerIdAndDeliveryMethod(customerId, deliveryMethod);
            return orders.thenApply(order -> order.stream().map(OrderMapper::toOrderDto).collect(Collectors.toList()));
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            throw new InternalServerException("Error retrieveForCustomerIdAndDeliveryMethod ");
        }

    }


    /**
     * @param customerId
     * @return
     */
    @CachePut(key = "3")
    public CollectionModel<Order> retrieveOrdersForCustomerId(Long customerId) {
        try {
            List<Order> orders = orderRepository.findByCustomerId(customerId);
            for (final Order order : orders) {
                Link selfLink = linkTo(methodOn(OrderController.class).getOrderById(customerId, order.getId())).withSelfRel();
                order.add(selfLink);
            }
            Link link = linkTo(methodOn(OrderController.class).getOrdersForCustomer(customerId)).withSelfRel();
            CollectionModel<Order> result = CollectionModel.of(orders, link);
            return result;
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            throw new InternalServerException("Error retrieveOrdersForCustomerId ");
        }
    }

    /**
     * @param customerId
     * @param orderId
     * @return
     */
    @Cacheable(key = "1")
    public Order retrieveOrderForId(Long customerId, Long orderId) {
        try {
            return orderRepository.findByCustomerIdAndId(customerId, orderId);
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            throw new InternalServerException("Error retrieveOrderForId ");
        }
    }


    /**
     * @param orderId
     * @return
     */
    @Transactional
    @CacheEvict(key = "1")
    public boolean removeOrderForId(Long orderId) {
        try {
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new DataNotFoundException("Order not found for this id :: " + orderId));
            orderRepository.delete(order);
            return Boolean.TRUE;
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            throw new InternalServerException("Error removeOrder ");
        }

    }
}
