package com.carrefour.javakata.service;


import com.carrefour.javakata.exception.DataNotFoundException;
import com.carrefour.javakata.exception.InternalServerException;
import com.carrefour.javakata.model.domain.Customer;
import com.carrefour.javakata.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Service
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"cacheCustomer"})
public class CustomerService {
    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);
    private final CustomerRepository customerRepository;


    /**
     * @return
     */
    @Cacheable(key = "1")
    public List<Customer> retrieveAll() {
        try {
            return customerRepository.findAll();
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            throw new InternalServerException("Error retrieveAll ");
        }
    }

    /**
     * @param customerId
     * @return
     * @throws DataNotFoundException
     */
    @CachePut(key = "2")
    public Customer retrieveForId(Long customerId) throws DataNotFoundException {
        try {
            return customerRepository.findById(customerId).orElseThrow(() -> new DataNotFoundException("Customer not found for this id :: " + customerId));
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            throw new InternalServerException("Error retrieveForId ");
        }
    }

    public void save(List<Customer> customers) {
        customerRepository.saveAll(customers);
    }
}
