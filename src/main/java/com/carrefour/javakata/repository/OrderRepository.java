package com.carrefour.javakata.repository;

import com.carrefour.javakata.enums.DeliveryMethod;
import com.carrefour.javakata.model.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Async
    CompletableFuture<List<Order>> findByCustomerIdAndDeliveryMethod(Long customerId, DeliveryMethod deliveryMethod);

    List<Order> findByCustomerId(Long customerId);

    Order findByCustomerIdAndId(Long customerId, Long orderId);
}
