package com.carrefour.javakata.converter;

import com.carrefour.javakata.enums.DeliveryMethod;
import jakarta.persistence.AttributeConverter;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
public class DeliveryMethodConverter implements AttributeConverter<DeliveryMethod, Integer> {
    @Override
    public Integer convertToDatabaseColumn(DeliveryMethod deliveryMethod) {
        try {
            return switch (deliveryMethod) {
                case DRIVE -> 1;
                case DELIVERY -> 2;
                case DELIVERY_TODAY -> 3;
                case DELIVERY_ASAP -> 4;
                default -> throw new IllegalArgumentException(deliveryMethod + " not supported.");
            };
        } catch (IllegalArgumentException e) {
            return null;
        }

    }

    @Override
    public DeliveryMethod convertToEntityAttribute(Integer dbData) {
        try {
            return switch (dbData) {
                case 1 -> DeliveryMethod.DRIVE;
                case 2 -> DeliveryMethod.DELIVERY;
                case 3 -> DeliveryMethod.DELIVERY_TODAY;
                case 4 -> DeliveryMethod.DELIVERY_ASAP;
                default -> throw new IllegalArgumentException(dbData + " not supported.");
            };
        } catch (IllegalArgumentException e) {
            return null;
        }

    }
}
