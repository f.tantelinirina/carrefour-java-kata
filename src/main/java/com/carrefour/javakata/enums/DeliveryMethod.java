package com.carrefour.javakata.enums;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
public enum DeliveryMethod {
    DRIVE(1), DELIVERY(2), DELIVERY_TODAY(3), DELIVERY_ASAP(4);
    int deliveryId;

    private DeliveryMethod(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public int getDeliveryId() {
        return deliveryId;
    }
}
