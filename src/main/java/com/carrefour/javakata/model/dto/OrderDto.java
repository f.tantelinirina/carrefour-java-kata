package com.carrefour.javakata.model.dto;

import com.carrefour.javakata.enums.DeliveryMethod;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto implements Serializable {

    private String numeroOrder;
    private Long orderId;
    private Long customerId;
    private DeliveryMethod deliveryMethod;
    private Date deliveryDate;
    private Integer deliveryHour;
}
