package com.carrefour.javakata.model.domain;

import com.carrefour.javakata.converter.DeliveryMethodConverter;
import com.carrefour.javakata.enums.DeliveryMethod;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;


/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "kata_order")
public class Order  extends RepresentationModel<Order> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Schema(description = "Unique identifier of the order", required = true)
    private Long id;

    @Column(name = "numero", length = 200, nullable = false)
    @Schema(description = "numero of order", required = true)
    private String numero;

    @Convert(converter = DeliveryMethodConverter.class)
    private DeliveryMethod deliveryMethod;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "delivery_date", nullable = false)
    @Schema(description = "creationDate of company", required = true)
    private Date deliveryDate;

    @Column(name = "delivery_hour", nullable = false)
    @Schema(description = "creationDate of company", required = true)
    private Integer deliveryHour;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

}
