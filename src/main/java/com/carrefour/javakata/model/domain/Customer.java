package com.carrefour.javakata.model.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;


/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "kata_customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Schema(description = "Unique identifier of the Customer", required = true)
    private Long id;

    @Column(name = "first_name", length = 200, nullable = false)
    @Schema(description = "first name", required = true)
    private String firstName;

    @Column(name = "last_name", length = 200, nullable = false)
    @Schema(description = "last name", required = true)
    private String lastName;

    @Column(name = "full_address", length = 200, nullable = false)
    @Schema(description = "fullAddress of customer", required = true)
    private String fullAddress;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @Column(name = "creation_date", length = 200, nullable = false)
    @Schema(description = "creationDate of customer", required = true)
    private Date creationDate;

    @Fetch(FetchMode.JOIN)
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @Schema(description = "Collection of command for customer.")
    private Set<Order> orders;
}
