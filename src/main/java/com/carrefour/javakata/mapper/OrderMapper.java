package com.carrefour.javakata.mapper;

import com.carrefour.javakata.model.domain.Order;
import com.carrefour.javakata.model.dto.OrderDto;
import org.springframework.beans.BeanUtils;

import java.util.Objects;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
public class OrderMapper {

    public static final OrderDto toOrderDto(Order order) {
        OrderDto orderDto = BeanUtils.instantiateClass(OrderDto.class);
        if (Objects.nonNull(order)) {
            orderDto.setOrderId(order.getId());
            orderDto.setNumeroOrder(order.getNumero());
            orderDto.setCustomerId(order.getCustomer().getId());
            orderDto.setDeliveryDate(order.getDeliveryDate());
            orderDto.setDeliveryHour(order.getDeliveryHour());
            orderDto.setDeliveryMethod(order.getDeliveryMethod());
        }
        return orderDto;
    }
}
