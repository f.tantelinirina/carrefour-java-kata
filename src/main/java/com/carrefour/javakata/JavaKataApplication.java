package com.carrefour.javakata;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author : t.randrianarisoa
 * @project : java-kata
 * @created on  : 06/04/2024
 */
@SpringBootApplication
@EnableTransactionManagement
@OpenAPIDefinition(info = @Info(title = "JavaKata API", version = "1.0", description = "API REST JavaKata application"))
public class JavaKataApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaKataApplication.class, args);
    }
}
