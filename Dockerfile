FROM openjdk:21-jdk-alpine
MAINTAINER baeldung.com
COPY target/java-kata-0.0.1-SNAPSHOT.jar java-kata-0.0.1.jar
ENTRYPOINT ["java","-jar","/java-kata-0.0.1.jar"]